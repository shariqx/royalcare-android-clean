package com.tradr.royalcare.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tradr.royalcare.R;
import com.tradr.royalcare.adaptor.TransactionAdaptor;
import com.tradr.royalcare.constant.Constants;
import com.tradr.royalcare.fragment.FuturesFragment;
import com.tradr.royalcare.fragment.SellFragment;
import com.tradr.royalcare.fragment.TransactionDetailFragment;
import com.tradr.royalcare.helper.PortfolioHelper;
import com.tradr.royalcare.model.TransactionData;
import com.tradr.royalcare.model.TransactionResponse;
import com.tradr.royalcare.network.ApiClient;
import com.tradr.royalcare.network.ApiInterface;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by priyanka on 11/18/17.
 */

public class TransactionDetailActivity extends AppCompatActivity {
    private int position;
    private Double currentCost;
    private String ltp;
    private String profit;
    private String profitPerc;
    private String cost;
    private int quantity;
    private String price;
    private String symbol;
    private String stockName;
    private String action;
    private double profit_value;

    private ViewPager viewPager;
    private TextView stockNameTV, currentCostTV, ltpTV, profitTV, profitPercTV, costTV, quantityTV, priceTV,quantityTextTV;
    private TextView profittext;
    private TextView symbolTV, actionTV;
    private ImageView actionIconIV;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_detail);


        position = getIntent().getIntExtra("position",0);
        Log.d("gettinghere",position+"");
        quantityTextTV = (TextView)findViewById(R.id.quantitytext);
        stockNameTV = (TextView)findViewById(R.id.stock_name);
        currentCostTV = (TextView)findViewById(R.id.current_cost);
        priceTV = (TextView)findViewById(R.id.price);
        ltpTV = (TextView)findViewById(R.id.ltp);
        //actionTV = (TextView)findViewById(R.id.action);
        profitTV = (TextView)findViewById(R.id.profit);
        profittext = (TextView)findViewById(R.id.profittext);
        profitPercTV = (TextView)findViewById(R.id.profit_perc);
        quantityTV = (TextView)findViewById(R.id.quantity);
        //actionIconIV = (ImageView)findViewById(R.id.action_icon);

        loadData();
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);



    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Bundle bundle = new Bundle();
        bundle.putString("mode", "buy");
        bundle.putInt("position",position+1);
        Fragment bfragment = new TransactionDetailFragment();
        bfragment.setArguments(bundle);
        adapter.addFragment(bfragment, "Current");
        viewPager.setAdapter(adapter);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    private void loadData()
    {
        Log.d("in ", "load data");
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<TransactionResponse> call = apiService.getTransactions(Constants.USERID);
        call.enqueue(new Callback<TransactionResponse>() {

            @Override
            public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {
                try {
                    Log.d("In here","I am");
                    List<TransactionData> movies = response.body().getTransaction();
                    movies = PortfolioHelper.getGroupedTransactionForListing(movies, "mode");
                    position+=1;
                    Log.d("position",position+"");
                    TransactionData t = movies.get(position);
                    Log.d("simbolhere",t.getSymbol());

                    ltp = t.getLTP();
                    price = t.getPrice();
                    Log.d("price fetched", price);
                    t.setCurrentCost(Double.parseDouble(t.getPrice())*Double.parseDouble(t.getQuantity()));
                    Log.d("transaction1",t.getPrice()+" "+t.getProfit()+" "+t.getQuantity()+" "+t.getLTP()+" "+t.getCurrentCost());
                    profitPerc = String.format("%.2f",t.getProfitPerc());
                    quantity = Integer.valueOf(t.getQuantity());
                    symbol = t.getSymbol();
                    stockName = t.getStockName();
                    action = t.getAction();
                    cost = t.getCost();
                    currentCost = Math.abs(t.getCurrentCost());

                    profit_value = Double.parseDouble(ltp)*quantity - currentCost;

                    Log.d("ltp",ltp);

                    stockNameTV.setText(stockName);
                    currentCostTV.setText("₹ "+String.format("%.2f",currentCost));
                    Log.d("profit here",t.getProfit()+"");
                    if(profit_value < 0)
                    {
                        profit = String.format("%.2f",Math.abs(profit_value));
                        profittext.setText("Net Loss");
                    }
                    else
                    {
                        profit = String.format("%.2f",profit_value);
                    }
                    profitTV.setText("₹ "+profit);
                    Log.d("profitvalue",profit);
                    double percentage = Math.abs(profit_value)/(Double.parseDouble(ltp)*quantity)*100;

                    profitPercTV.setText(String.format("%.2f",percentage)+"%");
                    priceTV.setText("₹ "+price);
                    if(quantity<0)
                    {
                        quantity = Math.abs(quantity);
                        quantityTextTV.setText("Sold");
                    }
                    quantityTV.setText(quantity+"");
                    //actionTV.setText(action);
                    ltpTV.setText("₹ "+ltp);
                    String buy_uri = "@drawable/buy_icon";
                    String sell_uri = "@drawable/sell_icon";

                    if(action.equals("buy"))
                    {
                        int imageResource = getResources().getIdentifier(buy_uri, null, getPackageName());
                        Drawable res = getResources().getDrawable(imageResource);
                        actionIconIV.setImageDrawable(res);
                    }
                    else
                    {
                        int imageResource = getResources().getIdentifier(sell_uri, null, getPackageName());
                        Drawable res = getResources().getDrawable(imageResource);
                        //actionIconIV.setImageDrawable(res);
                    }


                } catch (Exception ex) {
                    ex.printStackTrace();

                }
            }

            @Override
            public void onFailure(Call<TransactionResponse> call, Throwable t) {
                // Log error here since request failed
                Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
                Log.d("myapp", "Something", t);
            }
        });
    }

}
