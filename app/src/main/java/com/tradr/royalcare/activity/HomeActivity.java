package com.tradr.royalcare.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.BoomButtonBuilder;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;
import com.tradr.royalcare.R;
import com.tradr.royalcare.adaptor.HomeRecViewAdaptor;
import com.tradr.royalcare.model.HomeActivityCardData;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    private GridLayoutManager lLayout;
    private BoomMenuButton bmb;
    private FirebaseAuth auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        List<HomeActivityCardData> rowListItem = getAllItemList();
        lLayout = new GridLayoutManager(HomeActivity.this, 1);
        RecyclerView rView = (RecyclerView) findViewById(R.id.home_rec_view);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);
        HomeRecViewAdaptor rcAdapter = new HomeRecViewAdaptor(HomeActivity.this, rowListItem);
        rView.setAdapter(rcAdapter);
        bmb =  (BoomMenuButton) findViewById(R.id.bmb);
        addMenuButtons();
        auth = FirebaseAuth.getInstance();


    }

    /*
    @Override

    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.bmb) {

        }
        return true;
    }
    */


    private void addMenuButtons()
    {
        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
            switch(i)
            {
                case 0: TextOutsideCircleButton.Builder builder = new TextOutsideCircleButton.Builder()
                        .normalImageRes(R.drawable.stock_icon2)
                        .normalText("Stock")
                        .listener(new OnBMClickListener() {
                            @Override
                            public void onBoomButtonClick(int index) {
                                Intent intent = new Intent(getApplicationContext(), StockActivity.class);
                                startActivity(intent);
                                //Toast.makeText(HomeActivity.this, "Clicked", Toast.LENGTH_SHORT).show();
                            }
                        });
                        bmb.addBuilder(builder);
                        break;
                case 1: builder = new TextOutsideCircleButton.Builder()
                        .normalImageRes(R.drawable.portfolio_icon)
                        .normalText("Portfolio")
                        .listener(new OnBMClickListener() {
                            @Override
                            public void onBoomButtonClick(int index) {
                                Intent intent = new Intent(getApplicationContext(), PortfolioActivity.class);
                                startActivity(intent);
                                //Toast.makeText(HomeActivity.this, "Clicked", Toast.LENGTH_SHORT).show();
                            }
                        });
                        bmb.addBuilder(builder);
                        break;
                case 4: //Sign out
                        builder = new TextOutsideCircleButton.Builder()
                            .normalImageRes(R.drawable.logout)
                            .normalText("Logout")
                                .listener(new OnBMClickListener() {
                                    @Override
                                    public void onBoomButtonClick(int index) {
                                        auth.signOut();

                                        // this listener will be called when there is change in firebase user session
                                        FirebaseAuth.AuthStateListener authListener = new FirebaseAuth.AuthStateListener() {
                                            @Override
                                            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                                                FirebaseUser user = firebaseAuth.getCurrentUser();
                                                if (user == null) {
                                                    // user auth state is changed - user is null
                                                    // launch login activity
                                                    startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                                                    finish();
                                                }
                                            }
                                        };

                                        Toast.makeText(HomeActivity.this, "Logging out...", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                        startActivity(intent);
                                    }
                                });
                        bmb.addBuilder(builder);
                        break;
                default:    builder = new TextOutsideCircleButton.Builder()
                            .normalImageRes(R.drawable.portfolio_icon)
                            .normalText("Extra")
                        .listener(new OnBMClickListener() {
                            @Override
                            public void onBoomButtonClick(int index) {
                                //Toast.makeText(HomeActivity.this, "Clicked", Toast.LENGTH_SHORT).show();
                            }
                        });
                        bmb.addBuilder(builder);
                        break;

            }

        }

    }



    private List<HomeActivityCardData> getAllItemList() {

        List<HomeActivityCardData> allItems = new ArrayList<HomeActivityCardData>();
        allItems.add(new HomeActivityCardData(getString(R.string.stock_title), R.drawable.home_stock_icon));
        allItems.add(new HomeActivityCardData(getString(R.string.portfolio_title), R.drawable.home_portfolio_icon));


//        allItems.add(new HomeActivityCardData(getString(R.string.life_title), R.drawable.life_icon));
//        allItems.add(new HomeActivityCardData(getString(R.string.home_title), R.drawable.home_icon));
//        allItems.add(new HomeActivityCardData(getString(R.string.holiday_title), R.drawable.holiday_icon));
//        allItems.add(new HomeActivityCardData(getString(R.string.education_title), R.drawable.education_icon));
//        allItems.add(new HomeActivityCardData(getString(R.string.tax_title), R.drawable.tax_icon));
//        allItems.add(new HomeActivityCardData(getString(R.string.planner_title), R.drawable.planner_icon));
        return allItems;
    }
}
