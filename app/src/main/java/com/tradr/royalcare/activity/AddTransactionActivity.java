package com.tradr.royalcare.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tradr.royalcare.R;
import com.tradr.royalcare.constant.Constants;
import com.tradr.royalcare.fragment.SignalPopup;
import com.tradr.royalcare.model.LiveData;
import com.tradr.royalcare.model.LiveDataResponse;
import com.tradr.royalcare.network.ApiClient;
import com.tradr.royalcare.network.ApiInterface;
import com.tradr.royalcare.network.NetworkCall;
import com.tradr.royalcare.util.AlertDialogue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddTransactionActivity extends AppCompatActivity implements View.OnClickListener {

    private ApiInterface mAPIService;
    DatePickerDialog datePickerDialog;

    //Stock information TextViews
    protected String stockName;
    protected String pos;
    protected TextView target1;
    protected TextView target2;
    protected TextView target3;
    protected TextView stopLoss;

    //final Spinner s =(Spinner) findViewById(R.id.spinner_stock);
    Spinner buySellSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_transaction);
        final Spinner s = (Spinner) findViewById(R.id.spinner_stock);
        buySellSpinner = (Spinner) findViewById(R.id.spinner_buy_sell);
        //Initializing TextViews

        setSymbolListFromResponse(s);
        EditText date = (EditText) findViewById(R.id.txt_trans_date);
        EditText quantity = (EditText) findViewById(R.id.trans_quantity);
        final EditText price = (EditText) findViewById(R.id.trans_price);
        date.setOnClickListener(this);
        quantity.setOnClickListener(this);
        price.setOnClickListener(this);
        pos = "N/A";
        //Adding itemchange listener on Spinner to show respective Signal/Position
        //Of the selected symbol from server
        s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, final int position, long id) {
                // 1) Fetch calls From server
                // 2) Set current calls from the LiveData fetched from server.
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                Call<LiveDataResponse> call = apiService.getLiveDataForSymbol(String.valueOf(s.getSelectedItem()));
                call.enqueue(new Callback<LiveDataResponse>() {
                    @Override
                    public void onResponse(Call<LiveDataResponse> call, Response<LiveDataResponse> response) {
                        try {
                            price.setText("");
                            List<LiveData> liveDataList = response.body().getResults();
                            //If list is empty then there's no signal available for the current one.... show Pos as not available.
                            if (liveDataList.isEmpty()) {
                                pos="N/A";
                                target1.setText("0.00");
                                target2.setText("0.00");
                                target3.setText("0.00");
                                stopLoss.setText("0.00");
                                return;
                            }
                            LiveData currentData = liveDataList.get(0);
                            pos=String.valueOf(currentData.getPos().toUpperCase());
                            price.setText(String.valueOf(currentData.getLtp()));
                            if (currentData.getPos().equalsIgnoreCase("buy")) {
                                target1.setText(String.valueOf(currentData.getBtarget1()) + SignalPopup.isShowTick(currentData.isBT1()));
                                target2.setText(String.valueOf(currentData.getBtarget2()) + SignalPopup.isShowTick(currentData.isBT2()));
                                target3.setText(String.valueOf(currentData.getBtarget3()) + SignalPopup.isShowTick(currentData.isBT3()));
                                stopLoss.setText(String.valueOf(currentData.getBstoploss()));
                            } else if (currentData.getPos().equalsIgnoreCase("sell")) {
                                target1.setText(String.valueOf(currentData.getStarget1()) + SignalPopup.isShowTick(currentData.isST1()));
                                target2.setText(String.valueOf(currentData.getStarget2()) + SignalPopup.isShowTick(currentData.isST2()));
                                target3.setText(String.valueOf(currentData.getStarget3()) + SignalPopup.isShowTick(currentData.isST3()));
                                stopLoss.setText(String.valueOf(currentData.getSstoploss()));
                            } else if (currentData.getPos().equalsIgnoreCase("none")) {
                                pos="N/A";
                                target1.setText("0.00");
                                target2.setText("0.00");
                                target3.setText("0.00");
                                stopLoss.setText("0.00");
                            }
                            //Toast.makeText(AddTransactionActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        } catch (Exception ex) {
                            Toast.makeText(AddTransactionActivity.this, "Error while retreiving Stocks", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<LiveDataResponse> call, Throwable t) {
                        // Log error here since request failed
                        Toast.makeText(AddTransactionActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
                        Log.d("myapp", "Something went wrong in calling the livedata...", t);
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });


        buySellSpinner = (Spinner) findViewById(R.id.spinner_buy_sell);

        buySellSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, final int position, long id) {
                if (!pos.equals(buySellSpinner.getSelectedItem())) {
                    AlertDialogue ad = new AlertDialogue();
                    ad.msg = String.format("Position call for %s is %s. It is recommended to hold until next call. Do you still want to %s it?"
                            , "this stock", pos, buySellSpinner.getSelectedItem());
                    ad.show(getFragmentManager(), "Alert");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });


    }


    private void setSymbolListFromResponse(Spinner s) {
        try {
            ApiInterface apiService = ApiClient.getStringClient().create(ApiInterface.class);
            Call<ResponseBody> call = apiService.getSymbolListCSV();
            NetworkCall nc = new NetworkCall(s, getApplicationContext());
            nc.execute(call);
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_transaction, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save_transaction) {
            //Some validations first...
            //get the selected signal for the stock, if it's buy and otherwise is selected in spinner then show
            //warning.. if signal is not available then let user add...
            //showAlertDialogButtonClicked(null,"","");
            //Sending entered information to the server...
            mAPIService = ApiClient.getStringClient().create(ApiInterface.class);

            //Get data to be passed to the server in the post request..
            Spinner op = (Spinner) findViewById(R.id.spinner_buy_sell);
            String op_text = (String) op.getSelectedItem();
            Spinner symbol_tv = (Spinner) findViewById(R.id.spinner_stock);
            String symbol = (String) symbol_tv.getSelectedItem();
            EditText price_et = (EditText) findViewById(R.id.trans_price);
            Double price = Double.valueOf((String.valueOf(price_et.getText())));
            EditText quantity_et = (EditText) findViewById(R.id.trans_quantity);
            Integer quantity = Integer.valueOf((String.valueOf(quantity_et.getText())));
            EditText date = (EditText) findViewById(R.id.txt_trans_date);
            SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
            Date d = null;
            try {
                d = sdf.parse(date.getText().toString());
            } catch (Exception e) {
            }//TODO
            mAPIService.addTransaction(Constants.USERID, op_text, symbol, quantity, price, d.getTime()).enqueue(new Callback<ResponseBody>() {
                String TAG = "MyApp";

                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        try {
                            Log.i(TAG, "post submitted to API." + response.body().string());
                            Intent intent = new Intent(getApplicationContext(),PortfolioActivity.class);
                            startActivity(intent);
                        } catch (Exception e) {
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e(TAG, "Unable to submit post to API.");
                }
            });
        } else if (item.getItemId() == R.id.action_delete_transaction) {
            finish();
        }
        return true;
    }

    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.trans_quantity ||
                view.getId() == R.id.trans_price) {
            EditText et = (EditText) view.findViewById(view.getId());
            et.setHint("");
        }
        if (view.getId() == R.id.txt_trans_date) {
            final Calendar c = Calendar.getInstance();
            final EditText date = (EditText) view.findViewById(view.getId());
            int mYear = c.get(Calendar.YEAR); // current year
            int mMonth = c.get(Calendar.MONTH); // current month
            int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
            // date picker dialog

            datePickerDialog = new DatePickerDialog(AddTransactionActivity.this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            // set day of month , month and year value in the edit text
                            date.setText(dayOfMonth + "/"
                                    + (monthOfYear + 1) + "/" + year);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
    }

}