package com.tradr.royalcare.adaptor;

import android.animation.ValueAnimator;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tradr.royalcare.R;
import com.tradr.royalcare.activity.TransactionDetailActivity;
import com.tradr.royalcare.model.TransactionData;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Shariq on 6/5/2017.
 */

public class TransactionAdaptor extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<TransactionData> mTransaction;
    private static final int FOOTER_VIEW = 1;
    private static int currentPosition = 0;
    private Context context;
    private Boolean isViewExpanded = false;
    private int originalHeight = 0;
    static final float OPTIONS_AREA_PROPORTION = 0.5f;
    static final float REMOVE_ITEM_THRESHOLD = 0.6f;


    public class FooterViewHolder extends RecyclerView.ViewHolder {
        private TextView label;
        private TextView value;

        public FooterViewHolder(final View itemView) {
            super(itemView);
            label = (TextView) itemView.findViewById(R.id.summary_label);
            value = (TextView) itemView.findViewById(R.id.summary_value);

        }
    }

    public TransactionAdaptor() {

    }

    public TransactionAdaptor(List<TransactionData> Transaction) {
        this.mTransaction = Transaction;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.portfolio_transaction_card,
                parent, false);
        TransactionViewHolder vh = new TransactionViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder1, final int position) {

        if (holder1 instanceof TransactionViewHolder) {

            final TransactionViewHolder holder = (TransactionViewHolder) holder1;
            if(mTransaction.size() > 0) {
                TransactionData t = mTransaction.get(position);
                Log.d("t data ", t.toString());  //priyanka

                holder.price.setText("₹" + t.getLTP());
                Double currentCost = Double.valueOf(t.getQuantity()) * Double.valueOf(t.getLTP());
                t.setCurrentCost(currentCost);
                //holder.total_price.setText("₹"+ String.format("%.2f", currentCost));

                //holder.stock_name.setText(t.getStockName().length()
                //        > 15 ? t.getStockName().substring(0, 15) + "..." : t.getStockName());
                holder.stock_name.setText(t.getSymbol());
                // holder.bprice.setText(String.format("%.2f", Double.valueOf(t.getPrice()))); //Priyanka changed
                holder.units.setText("" + t.getQuantity());
                //holder.cost.setText(String.format("%.2f", Double.valueOf(t.getCost()))); //Priyanka Changed

                //If profit is negative, then set the Name's background color to red...
                double prof = Double.valueOf(t.getProfit());
                double profperc = Double.valueOf(t.getProfit()/Double.valueOf(t.getLTP()));
                t.setProfitPerc(profperc);
                if (Math.signum(prof) == -1.0d) {
                    holder.profit.setTextColor(Color.RED);
                    holder.profittext.setTextColor(Color.RED);
                    holder.profittext.setText("Loss (%)");
                    profperc = Math.abs(profperc);
                    prof = Math.abs(prof);

                    //holder.stock_name.setBackgroundColor(Color.RED);
                }
                Log.d("cccurent",t.getCurrentCost()+"");
                prof = Double.parseDouble(t.getLTP())-Double.parseDouble(t.getPrice()) ;
                prof *= Double.parseDouble(t.getQuantity());
                profperc = prof/(Double.parseDouble(t.getLTP())*Double.parseDouble(t.getQuantity()))*100;
                Log.d("ppprofit",prof+"");
                holder.profit.setText("₹" + String.format("%.2f", prof) + "\n(" + String.format("%.2f", profperc) + "%)");
                holder.stock_full_name.setText(t.getStockName());

                if (Integer.parseInt(t.getQuantity()) < 0) {
                    holder.units.setText(t.getQuantity().substring(1));
                    holder.quantitytext.setText("Sold");
                }

            }

        }
    }

    public void removeItem(int position) {
        mTransaction.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(TransactionData item, int position) {
        mTransaction.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }

    public TransactionData getItem(int position) {
        return mTransaction.get(position);
    }

    public List<TransactionData> getmTransaction() {
        return mTransaction;
    }

    public void setmTransaction(List<TransactionData> mTransaction) {
        this.mTransaction = mTransaction;
    }

    public class TransactionViewHolder extends RecyclerView.ViewHolder {

        protected TextView stock_name;
        protected TextView stock_full_name;
        protected TextView price;
        protected TextView profit;
        protected TextView units;
        protected RelativeLayout layout;
        public RelativeLayout viewBackground, viewForeground;

        protected TextView sellvaluetext, profittext, pricetext, quantitytext, signalValue;

        public TransactionViewHolder(View itemView) {
            super(itemView);
            stock_name = (TextView) itemView.findViewById(R.id.stock_name);
            price = (TextView) itemView.findViewById(R.id.price_value);
            profit = (TextView) itemView.findViewById(R.id.profit_value);
            units = (TextView) itemView.findViewById(R.id.units_value);
            //total_price = (TextView) itemView.findViewById(R.id.stock_price);
            layout = (RelativeLayout)itemView.findViewById(R.id.layout);
            viewBackground = (RelativeLayout)itemView.findViewById(R.id.view_background);
            viewForeground = (RelativeLayout) itemView.findViewById(R.id.view_foreground);
            stock_full_name = (TextView) itemView.findViewById(R.id.stock_full_name);
            signalValue = (TextView) itemView.findViewById(R.id.signal_value);

            price.setGravity(Gravity.CENTER);
            profit.setGravity(Gravity.CENTER);
            units.setGravity(Gravity.CENTER);
            signalValue.setGravity(Gravity.CENTER);

            sellvaluetext = (TextView) itemView.findViewById(R.id.sellvaluetext);
            pricetext = (TextView) itemView.findViewById(R.id.pricetext);
            profittext = (TextView) itemView.findViewById(R.id.profittext);
            quantitytext= (TextView) itemView.findViewById(R.id.quantitytext);

            sellvaluetext.setGravity(Gravity.CENTER);
            pricetext.setGravity(Gravity.CENTER);
            profittext.setGravity(Gravity.CENTER);
            quantitytext.setGravity(Gravity.CENTER);


        }
    }
    // Now the critical part. You have return the exact item count of your list
// I've only one footer. So I returned data.size() + 1
// If you've multiple headers and footers, you've to return total count
// like, headers.size() + data.size() + footers.size()

    @Override
    public int getItemCount() {
        if (mTransaction == null) {
            return 0;
        }

        if (mTransaction.size() == 0) {
            //Return 1 here to show nothing
            return 0;
        }

        // Add extra view to show the footer view
        return mTransaction.size();
    }

// Now define getItemViewType of your own.


}


