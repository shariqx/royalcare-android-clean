package com.tradr.royalcare.adaptor;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tradr.royalcare.R;
import com.tradr.royalcare.activity.PortfolioActivity;
import com.tradr.royalcare.activity.StockActivity;
import com.tradr.royalcare.model.HomeActivityCardData;

import java.util.List;

/**
 * Created by Shariq on 6/4/2017.
 */

public class HomeRecViewAdaptor extends RecyclerView.Adapter<HomeRecViewAdaptor.HomeRecViewAdaptorVH> {

    private List<HomeActivityCardData> HomeActivityCardDataList;
    private Context context;

    public HomeRecViewAdaptor(Context context, List<HomeActivityCardData> HomeActivityCardDataList) {
        this.HomeActivityCardDataList = HomeActivityCardDataList;
        this.context = context;
    }

    @Override
    public HomeRecViewAdaptorVH onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_card_layout, null);
        HomeRecViewAdaptorVH rcv = new HomeRecViewAdaptorVH(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(HomeRecViewAdaptorVH holder, int position) {
        holder.cardTitle.setText(HomeActivityCardDataList.get(position).getCardTitle());
        holder.cardImage.setImageResource(HomeActivityCardDataList.get(position).getCardImage());
    }

    @Override
    public int getItemCount() {
        return this.HomeActivityCardDataList.size();
    }


    public class HomeRecViewAdaptorVH extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView cardTitle;
        public ImageView cardImage;
        private Context context;

        public HomeRecViewAdaptorVH(View view) {
            super(view);
            view.setOnClickListener(this);
            cardTitle = (TextView) view.findViewById(R.id.card_title);
            cardImage = (ImageView) view.findViewById(R.id.card_image);
            context = view.getContext();
        }

        @Override
        public void onClick(View view) {
            if (getAdapterPosition() == 0) {
                Intent intent = new Intent(view.getContext(), StockActivity.class);
                context.startActivity(intent);
            } else if (getAdapterPosition() == 1) {
                Intent intent = new Intent(view.getContext(), PortfolioActivity.class);
                context.startActivity(intent);
            }/*
            else if (getAdapterPosition() == 2) {
                Intent intent = new Intent(view.getContext(), LifeActivity.class);
                context.startActivity(intent);
            }
            else if (getAdapterPosition() == 3) {
                Intent intent = new Intent(view.getContext(), HomeActivity.class);
                context.startActivity(intent);
            }
            else if (getAdapterPosition() == 4) {
                Intent intent = new Intent(view.getContext(), HolidayActivity.class);
                context.startActivity(intent);
            }
            else if (getAdapterPosition() == 5) {
                Intent intent = new Intent(view.getContext(), EducationActivity.class);
                context.startActivity(intent);
            }
            else if (getAdapterPosition() == 6) {
                Intent intent = new Intent(view.getContext(), TaxActivity.class);
                context.startActivity(intent);
            }
            else if (getAdapterPosition() == 7) {
                Intent intent = new Intent(view.getContext(), PlannerActivity.class);
                context.startActivity(intent);
            }*/ else {
                Toast.makeText(view.getContext(), R.string.coming_soon, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
