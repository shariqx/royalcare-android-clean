package com.tradr.royalcare.adaptor;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tradr.royalcare.R;
import com.tradr.royalcare.model.TransactionData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by priyanka on 11/20/17.
 */

public class TransactionDetailAdaptor extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private List<TransactionData> transactionList;
    private static final int FOOTER_VIEW = 1;
    private static int currentPosition = 0;
    private Context context;
    private Boolean isViewExpanded = false;
    private int originalHeight = 0;
    static final float OPTIONS_AREA_PROPORTION = 0.5f;
    static final float REMOVE_ITEM_THRESHOLD = 0.6f;



    public TransactionDetailAdaptor() {

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.portifolio_transaction_detail_card,
                parent, false);
        TransactionDetailAdaptor.TransactionViewHolder vh = new TransactionDetailAdaptor.TransactionViewHolder(itemView);
        return vh;
    }

    public TransactionDetailAdaptor(List<TransactionData> Transaction) {
        this.transactionList = Transaction;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder1, final int position) {

        if (holder1 instanceof TransactionDetailAdaptor.TransactionViewHolder ) {

            final TransactionDetailAdaptor.TransactionViewHolder holder = (TransactionDetailAdaptor.TransactionViewHolder) holder1;
            Log.d("transactionList",transactionList.toString());

            if(transactionList.size() > 0 && position < transactionList.size()) {

                TransactionData t = transactionList.get(position);
                Log.d("t detail data ", t.toString());  //priyanka
                Log.d("quantity",""+t.getQuantity());
                holder.price.setText("₹" + t.getLTP());
                Double currentCost = Double.valueOf(t.getQuantity()) * Double.valueOf(t.getPrice());
                t.setCurrentCost(currentCost);
                holder.total_price.setText("₹" + String.format("%.2f", currentCost));
                holder.units.setText("" + t.getQuantity());

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //2017-02-06T09:43:37.203
                simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                TimeZone tz = TimeZone.getTimeZone("IST");
                SimpleDateFormat destFormat = new SimpleDateFormat("dd MMM hh:mm a");
                destFormat.setTimeZone(tz);

                try {
                    String updatedtime = t.getDate();
                    Date myDate = simpleDateFormat.parse(updatedtime);
                    String formatted_date = destFormat.format(myDate);

                    holder.date.setText(formatted_date);
                } catch (Exception e) {
                    e.printStackTrace();
                }








                //holder.date.setText(t.getDate());

                if(t.getAction().equalsIgnoreCase("sell"))
                {
                    holder.unitstext.setText("Sold");
                }

                if(t.getProfit() < 0)
                {
                    holder.profittext.setText("Loss");
                    holder.profittext.setTextColor(Color.RED);
                    holder.profit.setTextColor(Color.RED);
                    holder.profit.setText("₹" +String.format("%.2f",(0-t.getProfit())));
                }
                else {
                    holder.profit.setText("₹" + String.format("%.2f", t.getProfit()));
                }

                Log.d("position",""+position);

            }

        }


    }


    public class TransactionViewHolder extends RecyclerView.ViewHolder {

        protected TextView price;
        protected TextView units;
        protected TextView total_price;
        protected RelativeLayout layout;
        protected TextView date;
        protected TextView profit;
        protected TextView profittext;
        protected TextView unitstext;

        public TransactionViewHolder(View itemView) {
            super(itemView);
            price = (TextView) itemView.findViewById(R.id.price_value);
            units = (TextView) itemView.findViewById(R.id.units_value);
            total_price = (TextView) itemView.findViewById(R.id.total_price);
            layout = (RelativeLayout)itemView.findViewById(R.id.layout);
            date = (TextView) itemView.findViewById(R.id.date);
            profit = (TextView)itemView.findViewById(R.id.profit_value);
            profittext = (TextView)itemView.findViewById(R.id.profittext);
            unitstext = (TextView)itemView.findViewById(R.id.quantitytext);
        }
    }

    @Override
    public int getItemCount() {
        if (transactionList == null) {
            return 0;
        }

        if (transactionList.size() == 0) {
            //Return 1 here to show nothing
            return 0;
        }

        // Add extra view to show the footer view
        return transactionList.size();
    }

// Now define getItemViewType of your own.



}


