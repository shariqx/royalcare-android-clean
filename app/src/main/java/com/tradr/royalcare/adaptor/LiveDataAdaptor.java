package com.tradr.royalcare.adaptor;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tradr.royalcare.R;
import com.tradr.royalcare.model.LiveData;
import com.tradr.royalcare.model.SignalData;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Shariq Shaikh on 2/7/2017.
 */

public class LiveDataAdaptor extends RecyclerView.Adapter<LiveDataAdaptor.LiveDataViewHolder> {

    private List<LiveData> mLiveData;

    public LiveDataAdaptor() {

    }

    public LiveDataAdaptor(List<LiveData> liveData) {
        this.mLiveData = liveData;
    }

    @Override
    public LiveDataAdaptor.LiveDataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.stock_livedata_row, parent, false);
//        itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                int a = view.getId();
//                objLiveData s = mLiveData.get(view.getId());
//                Toast.makeText(view.getContext(), s.getSignal() + "", Toast.LENGTH_SHORT).show();
//            }
//        });
        return new LiveDataAdaptor.LiveDataViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LiveDataAdaptor.LiveDataViewHolder holder, int position) {
        LiveData objLiveData = mLiveData.get(position);
        List<SignalData> signalList = objLiveData.getSignalList();
        Log.d("signalListSize",signalList.size()+"");
        String symbol = objLiveData.getSymbol();
        holder.symbol.setText(" " + symbol/*.substring(0,symbol.length() - 2)*/); //      //Remove '-I' from symbol.
        /*

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //2017-02-06T09:43:37.203

        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        TimeZone tz = TimeZone.getTimeZone("IST");
        SimpleDateFormat destFormat = new SimpleDateFormat("dd MMM hh:mm a");
        destFormat.setTimeZone(tz);

        try {
            Date myDate = simpleDateFormat.parse(objLiveData.getUpdate_time());
            String formatted_date = destFormat.format(myDate);

            holder.date.setText(formatted_date);
            holder.ltp.setText(String.valueOf(objLiveData.getLtp()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        */
        double incr = 0;
        if(objLiveData.getSignalList().get(0).getPos().equalsIgnoreCase("buy"))
        {
            incr = objLiveData.getSignalList().get(0).getBuyAbove();
            Log.d("buysignal","buy"+" "+objLiveData.getSymbol());
        }
        else
        {
            incr = objLiveData.getSignalList().get(0).getSellBelow();
            Log.d("buysignal","sell"+" "+objLiveData.getSymbol());

        }
        Log.d("incr",incr+"");


        holder.ltp.setText(String.valueOf(objLiveData.getLtp()));

        if (objLiveData.getPos().equalsIgnoreCase("buy")) {
            holder.signal.setImageResource(R.drawable.ic_trending_up_black_24px);
        } else if (objLiveData.getPos().equalsIgnoreCase("sell")) {
            holder.signal.setImageResource(R.drawable.ic_trending_down_black_24px);
        } else {
            //pos is none
            holder.signal.setImageResource(R.drawable.none_hyphen);
        }

        //holder.ltp.setText(objLiveData.getCurrent()); Temporarily adding BUY/SELL instead of current price ;)

        //String signal = objLiveData.getSignal();
        //String type = objLiveData.getType();
//
//        if (type.equalsIgnoreCase("prediction")) {
//
//            if(signal.equalsIgnoreCase("buy")) {
//                holder.ltp.setText("BUY");
//                holder.ltp.setTextColor(Color.GREEN);
//                holder.signal.setImageResource(R.drawable.green_arrow);
//            } else if (type.equalsIgnoreCase("prediction") &&
//                    signal.equalsIgnoreCase("sell")) {
//                holder.ltp.setTextColor(Color.RED);
//                holder.ltp.setText("SELL");
//                holder.signal.setImageResource(R.drawable.red_arrow);
//            }
//        }
//        else {
        holder.signal.setVisibility(View.VISIBLE);
        holder.ltp.setVisibility(View.VISIBLE);
//        }
    }

    public LiveData getItem(int position) {
        return mLiveData.get(position);
    }

    @Override
    public int getItemCount() {
        return mLiveData.size();
    }

    public List<LiveData> getmLiveData() {
        return mLiveData;
    }

    public void setmLiveData(List<LiveData> mLiveData) {
        this.mLiveData = mLiveData;
    }

    public class LiveDataViewHolder extends RecyclerView.ViewHolder {

        protected TextView symbol;
        protected TextView ltp;
        protected ImageView signal;
        protected TextView date;

        public LiveDataViewHolder(View itemView) {
            super(itemView);
            symbol = (TextView) itemView.findViewById(R.id.textViewSymbol);
            ltp = (TextView) itemView.findViewById(R.id.textViewLTP);
            signal = (ImageView) itemView.findViewById(R.id.imageViewSignal);
            //date = (TextView) itemView.findViewById(R.id.textViewCurrent);

        }
    }

    public void setFilter(List<LiveData> countryModels) {
        mLiveData = new ArrayList<>();
        mLiveData.addAll(countryModels);
        notifyDataSetChanged();
    }

}
