package com.tradr.royalcare.network;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Shariq Shaikh on 3/20/2017.
 */
public class NetworkCall extends AsyncTask<Call, Void, String> {

    Spinner s;
    String resp;
    Context c;

    public NetworkCall(Spinner s, Context c) {
        this.c = c;
        this.s = s;
    }

    @Override
    protected String doInBackground(Call... params) {
        try {
            Call<ResponseBody> call = params[0];
            Response<ResponseBody> response = call.execute();
            resp = response.body().string();
            return resp;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        ArrayAdapter<String> adapter;
        String[] split;
        split = result.split(",");
        List<String> list = new ArrayList<String>();
        for (String s : split) {
            list.add(s);
        }
        adapter = new ArrayAdapter<String>(c,
                android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s.setAdapter(adapter);
    }
}