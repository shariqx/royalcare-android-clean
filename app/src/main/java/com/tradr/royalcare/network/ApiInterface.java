package com.tradr.royalcare.network;

import com.tradr.royalcare.model.LiveDataResponse;
import com.tradr.royalcare.model.TransactionResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by JPatel on 22-09-16.
 */

public interface ApiInterface {
    @GET("get_live_data/")
    Call<LiveDataResponse> getLiveData();

    @POST("add_transaction/")
    @FormUrlEncoded
    Call<ResponseBody> addTransaction(@Field("user_id") int user_id, @Field("transaction_type") String transType, @Field("symbol") String symbol,
                                      @Field("quantity") int quantity, @Field("price") double price, @Field("date") long transDate);

    @POST("get_transactions/")
    @FormUrlEncoded
    Call<TransactionResponse> getTransactions(@Field("user_id") int user_id);

    @GET("get_symbol_list/")
    Call<ResponseBody> getSymbolListCSV();

    @POST("get_live_data_for_symbol/")
    @FormUrlEncoded
    Call<LiveDataResponse> getLiveDataForSymbol(@Field("symbol") String symbol);

    @POST("delete_transaction_for_user/")
    @FormUrlEncoded
    Call<ResponseBody> deleteTransactionForUser(@Field("user_id") int user_id , @Field("symbol") String symbol);

}
