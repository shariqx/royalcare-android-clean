package com.tradr.royalcare.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Shariq Shaikh on 2/6/2017.
 */

public class LiveData {
    /* runid, symbol, stockname, signaltype, stocktype, ltp, pos, isBt1,
           # isBt2, isBt3, isST1, isST2, isST3, buy_above,
           # sell_below, bt1, bt2, bt3, st1, st2, st3, bsl, ssl, update_time,
            signal_list - buy_above, sell_below, date, signal-pos */
    @SerializedName("symbol")
    private String symbol;
    @SerializedName("stockname")
    private String stockname;
    @SerializedName("signaltype")
    private String signaltype;
    @SerializedName("stocktype")
    private String stocktype;
    @SerializedName("ltp")
    private double ltp;
    @SerializedName("pos")
    private String pos;
    @SerializedName("isBt1")
    private boolean isBT1;
    @SerializedName("isBt2")
    private boolean isBT2;
    @SerializedName("isBt3")
    private boolean isBT3;
    /*
        isBt2, isBt3, isST1, isST2, isST3, buy_above,
                # sell_below, bt1, bt2, bt3, st1, st2, st3, bsl, ssl, update_time */
    @SerializedName("isST1")
    private boolean isST1;
    @SerializedName("isST2")
    private boolean isST2;
    @SerializedName("isST3")
    private boolean isST3;

    @SerializedName("buy_above")
    private double buy_above;
    @SerializedName("sell_below")
    private double sell_below;
    @SerializedName("bt1")
    private double btarget1;
    @SerializedName("bt2")
    private double btarget2;
    @SerializedName("bt3")
    private double btarget3;
    @SerializedName("st1")
    private double starget1;
    @SerializedName("st2")
    private double starget2;
    @SerializedName("st3")
    private double starget3;
    @SerializedName("bsl")
    private double bstoploss;
    @SerializedName("ssl")
    private double sstoploss;


    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    @SerializedName("update_time")
    private String update_time;

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getStockname() {
        return stockname;
    }

    public void setStockname(String stockname) {
        this.stockname = stockname;
    }

    public String getSignaltype() {
        return signaltype;
    }

    public void setSignaltype(String signaltype) {
        this.signaltype = signaltype;
    }

    public String getStocktype() {
        return stocktype;
    }

    public void setStocktype(String stocktype) {
        this.stocktype = stocktype;
    }

    public double getLtp() {
        return ltp;
    }

    public void setLtp(double ltp) {
        this.ltp = ltp;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public boolean isBT1() {
        return isBT1;
    }

    public void setBT1(boolean BT1) {
        isBT1 = BT1;
    }

    public boolean isBT2() {
        return isBT2;
    }

    public void setBT2(boolean BT2) {
        isBT2 = BT2;
    }

    public boolean isBT3() {
        return isBT3;
    }

    public void setBT3(boolean BT3) {
        isBT3 = BT3;
    }

    public boolean isST1() {
        return isST1;
    }

    public void setST1(boolean ST1) {
        isST1 = ST1;
    }

    public boolean isST2() {
        return isST2;
    }

    public void setST2(boolean ST2) {
        isST2 = ST2;
    }

    public boolean isST3() {
        return isST3;
    }

    public void setST3(boolean ST3) {
        isST3 = ST3;
    }

    public double getBuy_above() {
        return buy_above;
    }

    public void setBuy_above(double buy_above) {
        this.buy_above = buy_above;
    }

    public double getSell_below() {
        return sell_below;
    }

    public void setSell_below(double sell_below) {
        this.sell_below = sell_below;
    }

    public double getBtarget1() {
        return btarget1;
    }

    public void setBtarget1(double btarget1) {
        this.btarget1 = btarget1;
    }

    public double getBtarget2() {
        return btarget2;
    }

    public void setBtarget2(double btarget2) {
        this.btarget2 = btarget2;
    }

    public double getBtarget3() {
        return btarget3;
    }

    public void setBtarget3(double btarget3) {
        this.btarget3 = btarget3;
    }

    public double getStarget1() {
        return starget1;
    }

    public void setStarget1(double starget1) {
        this.starget1 = starget1;
    }

    public double getStarget2() {
        return starget2;
    }

    public void setStarget2(double starget2) {
        this.starget2 = starget2;
    }

    public double getStarget3() {
        return starget3;
    }

    public void setStarget3(double starget3) {
        this.starget3 = starget3;
    }

    public double getBstoploss() {
        return bstoploss;
    }

    public void setBstoploss(double bstoploss) {
        this.bstoploss = bstoploss;
    }

    public double getSstoploss() {
        return sstoploss;
    }

    public void setSstoploss(double sstoploss) {
        this.sstoploss = sstoploss;
    }

    @SerializedName("signal_list")
    private List<SignalData> signalList;

    public void setSignalList(List<SignalData> signalList) {this.signalList = signalList;}

    public List<SignalData> getSignalList() {return signalList;}
}

