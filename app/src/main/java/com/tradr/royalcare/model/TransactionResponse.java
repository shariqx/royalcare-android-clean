package com.tradr.royalcare.model;

/**
 * Created by Shariq on 6/5/2017.
 */

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TransactionResponse {

    @SerializedName("Transaction")
    private List<TransactionData> transaction = null;
    private final static long serialVersionUID = 5166597326015072679L;

    public List<TransactionData> getTransaction() {
        return transaction;
    }

    public void setTransaction(List<TransactionData> transaction) {
        this.transaction = transaction;
    }

}