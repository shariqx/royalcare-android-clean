package com.tradr.royalcare.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by priyanka on 11/21/17.
 */

public class SignalData {
    /*
    date, buy_above, sell_below, symbol, signal - pos
     */

    @SerializedName("buy_above")
    private double buy_above;
    @SerializedName("sell_below")
    private double sell_below;
    @SerializedName("symbol")
    private String symbol;
    @SerializedName("signal")
    private String pos;
    @SerializedName("date")
    private String date;

    public double getBuyAbove() {return buy_above;}

    public double getSellBelow() {return sell_below;}

    public String getSymbol() {return symbol;}

    public String getPos() {return pos;}

    public void setBuyAbove(double buy_above) {this.buy_above = buy_above;}

    public void setSellBelow(double sell_below) {this.sell_below = sell_below;}

    public void setSymbol(String symbol) {this.symbol = symbol;}

    public void setPos(String pos) {this.pos = pos;}

    public String getDate() {return date;}

    public void setDate(String date) {this.date = date;}
}
