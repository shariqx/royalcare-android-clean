package com.tradr.royalcare.model;

/**
 * Created by Shariq on 6/4/2017.
 */

public class HomeActivityCardData {
    private String cardTitle;
    private int cardImage;

    public HomeActivityCardData(String cardTitle, int cardImage) {
        this.cardTitle = cardTitle;
        this.cardImage = cardImage;
    }

    public String getCardTitle() {
        return cardTitle;
    }

    public void setCardTitle(String careTitle) {
        this.cardTitle = careTitle;
    }

    public int getCardImage() {
        return cardImage;
    }

    public void setCardImage(int cardImage) {
        this.cardImage = cardImage;
    }
}
