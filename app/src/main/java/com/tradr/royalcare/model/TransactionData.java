package com.tradr.royalcare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TransactionData implements Serializable {
    @SerializedName("id")
    @Expose
    private int id;
    public boolean pinned;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLTP() {
        return LTP;

    }

    public void setLTP(String LTP) {
        this.LTP = LTP;
    }

    public double getCurrentCost() {
        return currentCost;
        //Double.parseDouble(this.LTP) * Double.parseDouble(this.quantity)
    }

    public void setCurrentCost(double currentCost) {
        this.currentCost = currentCost;
    }

    private double profit;
    private double profitPerc;

    public double getProfit() {
        double profit = 0.0d;
        profit = Double.parseDouble(quantity)*Double.parseDouble(LTP);
        profit -= currentCost;
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public double getProfitPerc() {
        return profitPerc;
    }

    public void setProfitPerc(double profitPerc) {
        this.profitPerc = profitPerc;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    @SerializedName("ltp")
    @Expose
    private String LTP;

    private transient double currentCost;

    @SerializedName("name")
    @Expose
    private String stockName;

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("cost")
    @Expose
    private String cost;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("symbol")
    @Expose
    private String symbol;
    @SerializedName("userid")
    @Expose
    private int userid;
    @SerializedName("date")
    @Expose
    private String date;
    private final static long serialVersionUID = 8575617154111180252L;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}