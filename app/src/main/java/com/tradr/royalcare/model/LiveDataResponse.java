package com.tradr.royalcare.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Shariq Shaikh on 2/7/2017.
 */

public class LiveDataResponse {

    @SerializedName("LiveData")
    private List<LiveData> results;

    public List<LiveData> getResults() {
        return results;
    }

    public void setResults(List<LiveData> results) {
        this.results = results;
    }
}
