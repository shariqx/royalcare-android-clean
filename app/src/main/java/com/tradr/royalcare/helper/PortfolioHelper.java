package com.tradr.royalcare.helper;

import android.util.Log;

import com.tradr.royalcare.model.TransactionData;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Shariq Shaikh on 3/16/2017.
 */

public class PortfolioHelper {
    private static HashMap<String, ArrayList<TransactionData>> transMap;
    private static List<TransactionData> historyList;
    private static List<TransactionData> currentList;
    /**
     * @param transactionList
     * @param groupMode       -- BUY/SELL/BOTH
     * @return
     */
    public static List<TransactionData> getGroupedTransactionForListing(List<TransactionData> transactionList, String groupMode) {

        if (transactionList == null || transactionList.size() < 1)
            return transactionList;
        /* STEP 1 : Make a map of all the Transactions */
        transMap = new HashMap<String, ArrayList<TransactionData>>();
        historyList = new ArrayList<TransactionData>();
        currentList = new ArrayList<TransactionData>();
        List<TransactionData> groupedTrans = new ArrayList<TransactionData>();

        for (TransactionData td : transactionList) {
            Log.d("td",td.toString());

            if (!transMap.containsKey(td.getSymbol())) {
                ArrayList<TransactionData> s = new ArrayList<TransactionData>();
                //if(td.getAction().equalsIgnoreCase(groupMode))
                s.add(td);
                Log.d("Adding",td.getSymbol());
                transMap.put(td.getSymbol(), s);

                continue;
            }
            // if(td.getAction().equalsIgnoreCase(groupMode))
            transMap.get(td.getSymbol()).add(td);

        }
        Log.d("transMap",transMap.toString());

        /*STEP 2 :Iterate over each of the keys, and calculate their particulars */
        for (String key : transMap.keySet()) {
            double avg_price = 0.0d;
            int quantity = 0;
            double cost = 0.0d;
            int sell_quant = 0;
            double net_profit = 0.0d;
            double profit_perc = 0.0d;

            ArrayList<TransactionData> data = transMap.get(key);

            if (data.size() < 1) continue;
            for (TransactionData td : data) {
                Log.d("td here",td.toString());
                if (td.getAction().equalsIgnoreCase("buy"))
                    quantity += Double.parseDouble(td.getQuantity());
                else {
                    sell_quant += Double.parseDouble(td.getQuantity());
                    quantity -= Double.parseDouble(td.getQuantity());
                }
                //if (!td.getAction().equalsIgnoreCase(groupMode))
                  //  continue;
                cost += Double.parseDouble(td.getCost());
                avg_price += Double.parseDouble(td.getPrice())*Double.parseDouble(td.getQuantity());
                Log.d("praju",avg_price+"");
            }
            double currentCost = avg_price;
            Log.d("average p",avg_price+"");
            Log.d("total quantity",quantity+"");
            avg_price = avg_price / quantity;
            double ltp = Double.parseDouble(data.get(0).getLTP());
            TransactionData td = new TransactionData();
            td.setLTP(String.valueOf(ltp));
            td.setCurrentCost(avg_price*quantity);
            td.setSymbol(key);
            //Log.d("Avg price",avg_price+"");
            if (groupMode.equalsIgnoreCase("sell"))
                td.setQuantity(String.valueOf(sell_quant));
            else
                td.setQuantity(String.valueOf(quantity));


            net_profit = ltp*quantity - currentCost;
            profit_perc = net_profit/(ltp*quantity)*100;
            Log.d("net profit",net_profit+"");
            td.setProfit(net_profit);
            td.setProfitPerc(profit_perc);

            td.setCost(String.valueOf(cost));
            td.setPrice(String.format("%.2f",avg_price));
            Log.d("ghhh",avg_price+"");
            td.setAction(groupMode);
            td.setDate(new Date().toString());
            td.setStockName(data.get(0).getStockName());
            //Log.d("addingit",td.getSymbol());
            //Log.d("quantity",td.getQuantity()+"");
            if(quantity == 0) {
                historyList.add(td);

            }
            else
            {
                currentList.add(td);
            }
            Log.d("grouped price",td.getPrice()+"");
            groupedTrans.add(td);

        }
        return groupedTrans;
    }

    public static List<TransactionData> getHistoryList()
    {
        return historyList;
    }

    public static List<TransactionData> getCurrentList()
    {
        return currentList;
    }
    public static HashMap<String,ArrayList<TransactionData>> getTransactionDataList()
    {
        return transMap;
    }
}
