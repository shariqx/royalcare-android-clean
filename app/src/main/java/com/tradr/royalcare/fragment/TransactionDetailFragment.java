package com.tradr.royalcare.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.tradr.royalcare.R;
import com.tradr.royalcare.activity.TransactionDetailActivity;
import com.tradr.royalcare.adaptor.TransactionAdaptor;
import com.tradr.royalcare.adaptor.TransactionDetailAdaptor;
import com.tradr.royalcare.constant.Constants;
import com.tradr.royalcare.helper.PortfolioHelper;
import com.tradr.royalcare.helper.RecyclerItemTouchHelper;
import com.tradr.royalcare.model.TransactionData;
import com.tradr.royalcare.model.TransactionResponse;
import com.tradr.royalcare.network.ApiClient;
import com.tradr.royalcare.network.ApiInterface;
import com.tradr.royalcare.util.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by priyanka on 11/20/17.
 */

public class TransactionDetailFragment extends Fragment {
    private TransactionDetailAdaptor transAdaptor;
    private TransactionData transactionData;
    private RecyclerView recList;
    private HashMap<String,ArrayList<TransactionData>> transactionList;
    private Context context;
    static final float OPTIONS_AREA_PROPORTION = 0.5f;
    static final float REMOVE_ITEM_THRESHOLD = 0.6f;
    private LinearLayout coordinatorLayout;

    public TransactionDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        context = getContext();
        final View view = inflater.inflate(R.layout.transaction_detail_fragment, container, false);

        //recList.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(this, R.drawable.list_divider), true));

        recList = (RecyclerView) view.findViewById(R.id.transaction_list);
        coordinatorLayout = (LinearLayout)view.findViewById(R.id.coordinator_layout);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        recList.setItemAnimator(new DefaultItemAnimator());
        recList.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));

        // making http call and fetching menu json
        //TODO: Code to call another activity for showing the transaction full details will be called from here
        recList.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(final View view, int position) {
            }
        }));

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //Call<StockResponse> call = apiService.getPredictionOrSymbolJSON();
        Call<TransactionResponse> call = apiService.getTransactions(Constants.USERID);
        call.enqueue(new Callback<TransactionResponse>() {
            @Override
            public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {
                try {
                    List<TransactionData> thisa = response.body().getTransaction();
                    thisa = PortfolioHelper.getGroupedTransactionForListing(thisa, "mode");
                    TransactionData t = thisa.get(getArguments().getInt("position"));

                    String symbol = t.getSymbol();


                    HashMap<String,ArrayList<TransactionData>> movies = PortfolioHelper.getTransactionDataList();
                    Log.d("transMaphere",movies.toString());
                    Log.d("sumbol",symbol);
                    transAdaptor = new TransactionDetailAdaptor(movies.get(symbol));
                    recList.setAdapter(transAdaptor);
                    transactionList = movies;


                } catch (Exception ex) {
                    ex.printStackTrace();
                    Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TransactionResponse> call, Throwable t) {
                // Log error here since request failed
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();
                Log.d("myapp", "Something", t);
            }
        });

        return view;
    }



}