package com.tradr.royalcare.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tradr.royalcare.R;

/**
 * Created by Shariq on 6/4/2017.
 */


public class CashFragment extends Fragment {


    private RecyclerView recList;

    public CashFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.stock_cash_fragment, container, false);
        return view;
    }
}
