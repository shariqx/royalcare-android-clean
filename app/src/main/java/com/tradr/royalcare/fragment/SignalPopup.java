package com.tradr.royalcare.fragment;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tradr.royalcare.R;
import com.tradr.royalcare.model.LiveData;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by shariq on 11/25/2016.
 */

public class SignalPopup extends DialogFragment {

    static LiveData mLiveData;

    public static SignalPopup newInstance(LiveData liveData) {
        SignalPopup PredictionPopup = new SignalPopup();
        mLiveData = liveData;
        PredictionPopup.setStyle(DialogFragment.STYLE_NO_FRAME, android.R.style.Theme_DeviceDefault_Dialog);
        return PredictionPopup;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Remove the default background
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Inflate the new view with margins and background
        View v = null;
        String pos = mLiveData.getPos();
        if (pos.equalsIgnoreCase("buy"))
            v = inflater.inflate(R.layout.stock_pop_buy, container, false);
        else if (pos.equalsIgnoreCase("sell"))
            v = inflater.inflate(R.layout.stock_pop_sell, container, false);
        else if (pos.equalsIgnoreCase("none"))
            v = inflater.inflate(R.layout.stock_pop_nopos, container, false);
        try {
            showPredictionPopup(v, pos);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        // Set up a click listener to dismiss the popup if they click outside
        // of the background view
        v.findViewById(R.id.popup_root).setOnClickListener(new View.OnClickListener() {

            //Close the pop up on button click.
            @Override
            public void onClick(View v) {
                //dismiss();
            }
        });

        return v;
    }

    public void showPredictionPopup(View v, String pos) throws ParseException {
        //Common for all
        TextView symboltv = (TextView) v.findViewById(R.id.symbol);
        //TextView durationLbl = (TextView) v.findViewById(R.id.predictionDurationLbl);

        //TextView durationTxt = (TextView) v.findViewById(R.id.predictionDurationTxt); //Hidden data
        TextView symbolName = (TextView) v.findViewById(R.id.symbolname);
        TextView ltp = (TextView) v.findViewById(R.id.ltp);
        //TextView postxt = (TextView) v.findViewById(R.id.position); //Hidden data

        //durationTxt.setText(mLiveData.getSignaltype().toUpperCase()); //Hidden data
        symbolName.setText(mLiveData.getStockname());
        ltp.setText("₹ "+String.valueOf(mLiveData.getLtp()));
        //postxt.setText(pos.toUpperCase()); //Hidden data

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM hh:mm a");

        if (pos.equalsIgnoreCase("buy") || pos.equalsIgnoreCase("none")) {
            //TextView bTargets = (TextView) v.findViewById(R.id.bTargets); //Hidden data
            //TextView bStoploss = (TextView) v.findViewById(R.id.bStoploss); //Hidden data
            TextView buyAbove1 = (TextView) v.findViewById(R.id.buy_above1);
            TextView date1 = (TextView) v.findViewById(R.id.date1);
            TextView buyAbove2 = (TextView) v.findViewById(R.id.buy_above2);
            TextView date2 = (TextView) v.findViewById(R.id.date2);
            TextView buyAbove3 = (TextView) v.findViewById(R.id.buy_above3);
            TextView date3 = (TextView) v.findViewById(R.id.date3);
            ImageView signal1 = (ImageView) v.findViewById(R.id.buy_signal1);
            ImageView signal2 = (ImageView) v.findViewById(R.id.buy_signal2);
            ImageView signal3 = (ImageView) v.findViewById(R.id.buy_signal3);
            TextView updatetime = (TextView) v.findViewById(R.id.updatetime);

            Drawable greencircle = getResources().getDrawable(R.drawable.buy);
            Drawable redcircle = getResources().getDrawable(R.drawable.sell);

            Log.d("Signal list", mLiveData.getSignalList().get(0).toString());


            //update time
            String update_time = mLiveData.getUpdate_time();
            Log.d("updatetime",update_time);
            Date mydate1 = simpleDateFormat.parse(update_time);
            SimpleDateFormat current = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
            current.setTimeZone(TimeZone.getTimeZone("UTC"));
            String current_time = SimpleDateFormat.getDateTimeInstance().format(new Date());
            Date mydate2 = current.parse(current_time);
            current_time = simpleDateFormat.format(mydate2);
            Log.d("currentime", current_time);

            long difference = mydate2.getTime() - mydate1.getTime(); //miliseconds
            int extra = 330;
            int minutes = (int)difference/(1000*60) + extra;
            int hours = minutes/(60);
            int days = hours/24;

            Log.d("minutes",minutes+"");
            if(days == 1)
            {
                updatetime.setText("Updated "+days+" day ago");
            }
            else if (days > 1)
            {
                updatetime.setText("Updated "+days+" days ago");

            }
            else if (hours > 1)
            {
                updatetime.setText("Updated "+hours+" hours ago");
            }
            else if(hours == 1)
            {
                updatetime.setText("Updated "+hours+ "hour ago");
            }
            else if(minutes == 0)
            {
                updatetime.setText("Updated "+minutes+" minute ago");
            }
            else
            {
                updatetime.setText("Updated "+minutes+" minutes ago");
            }



            //signal 1
            String signal = mLiveData.getSignalList().get(0).getPos().toUpperCase();

            double buyAboveVal = 0;
            String date = "";
            if (pos.equalsIgnoreCase("buy")) {
                buyAboveVal = mLiveData.getSignalList().get(0).getBuyAbove();
            } else {
                buyAboveVal = mLiveData.getSignalList().get(0).getSellBelow();
            }

            TimeZone tz = TimeZone.getTimeZone("IST");
            SimpleDateFormat destFormat = new SimpleDateFormat("dd MMM hh:mm a");
            destFormat.setTimeZone(tz);

            date = mLiveData.getSignalList().get(0).getDate();
            try {
                Date myDate = simpleDateFormat.parse(date);
                date = destFormat.format(myDate);

            }
            catch(ParseException e)
            {
                e.printStackTrace();
            }

            date1.setText(date);
            if(!signal.equalsIgnoreCase("buy"))
            {
                signal1.setImageDrawable(redcircle);
            }
            buyAbove1.setText("₹"+ buyAboveVal);

            //signal 2
            signal = mLiveData.getSignalList().get(1).getPos().toUpperCase();

            if (pos.equalsIgnoreCase("buy")) {
                buyAboveVal = mLiveData.getSignalList().get(1).getBuyAbove();
            } else {
                buyAboveVal = mLiveData.getSignalList().get(1).getSellBelow();
            }
            date = mLiveData.getSignalList().get(1).getDate();

            try {
                Date myDate = simpleDateFormat.parse(date);
                date = destFormat.format(myDate);

            }
            catch(ParseException e)
            {
                e.printStackTrace();
            }

            date2.setText(date);
            if(!signal.equalsIgnoreCase("buy"))
            {
                signal2.setImageDrawable(redcircle);
            }
            buyAbove2.setText("₹"+ buyAboveVal);

            //signal 3
            signal = mLiveData.getSignalList().get(2).getPos().toUpperCase();

            if (pos.equalsIgnoreCase("buy")) {
                buyAboveVal = mLiveData.getSignalList().get(2).getBuyAbove();
            } else {
                buyAboveVal = mLiveData.getSignalList().get(2).getSellBelow();
            }
            date = mLiveData.getSignalList().get(2).getDate();

            try {
                Date myDate = simpleDateFormat.parse(date);
                date = destFormat.format(myDate);
            }
            catch(ParseException e)
            {
                e.printStackTrace();
            }

            date3.setText(date);
            if(!signal.equalsIgnoreCase("buy"))
            {
                signal3.setImageDrawable(redcircle);
            }
            buyAbove3.setText("₹"+ buyAboveVal);


            /*bTargets.setText(String.valueOf(bTarget1) + isShowTick(mLiveData.isBT1()) + "\n" +
                    bTarget2 + isShowTick(mLiveData.isBT2()) + "\n" +
                    bTarget3 + isShowTick(mLiveData.isBT3()));
            bStoploss.setText(String.valueOf(bStoploss_val));*/  //Hidden data
        }

        if (pos.equalsIgnoreCase("sell") || pos.equalsIgnoreCase("none")) {
            //TextView sTargets = (TextView) v.findViewById(R.id.sTargets); //Hidden data
            //TextView sStoploss = (TextView) v.findViewById(R.id.sStoploss); //Hidden data
            TextView buyAbove1 = (TextView) v.findViewById(R.id.buy_above1);
            TextView date1 = (TextView) v.findViewById(R.id.date1);
            TextView buyAbove2 = (TextView) v.findViewById(R.id.buy_above2);
            TextView date2 = (TextView) v.findViewById(R.id.date2);
            TextView buyAbove3 = (TextView) v.findViewById(R.id.buy_above3);
            TextView date3 = (TextView) v.findViewById(R.id.date3);
            ImageView signal1 = (ImageView) v.findViewById(R.id.buy_signal1);
            ImageView signal2 = (ImageView) v.findViewById(R.id.buy_signal2);
            ImageView signal3 = (ImageView) v.findViewById(R.id.buy_signal3);
            TextView updatetime = (TextView) v.findViewById(R.id.updatetime);


            Drawable greencircle = getResources().getDrawable(R.drawable.buy);
            Drawable redcircle = getResources().getDrawable(R.drawable.sell);


            Log.d("Signal list", mLiveData.getSignalList().get(0).toString());


            //update time
            String update_time = mLiveData.getUpdate_time();
            Log.d("updatetime",update_time);
            Date mydate1 = simpleDateFormat.parse(update_time);
            SimpleDateFormat current = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
            current.setTimeZone(TimeZone.getTimeZone("UTC"));
            String current_time = SimpleDateFormat.getDateTimeInstance().format(new Date());
            Date mydate2 = current.parse(current_time);
            current_time = simpleDateFormat.format(mydate2);
            Log.d("currentime", current_time);

            long difference = mydate2.getTime() - mydate1.getTime(); //miliseconds
            int extra = 330;
            int minutes = (int)difference/(1000*60) + extra;
            int hours = minutes/(60);
            int days = hours/24;

            Log.d("minutes",minutes+"");
            if(days == 1)
            {
                updatetime.setText("Updated "+days+" day ago");
            }
            else if (days > 1)
            {
                updatetime.setText("Updated "+days+" days ago");

            }
            else if (hours > 1)
            {
                updatetime.setText("Updated "+hours+" hours ago");
            }
            else if(hours == 1)
            {
                updatetime.setText("Updated "+hours+ "hour ago");
            }
            else if(minutes == 0)
            {
                updatetime.setText("Updated "+minutes+" minute ago");
            }
            else
            {
                updatetime.setText("Updated "+minutes+" minutes ago");
            }

            TimeZone tz = TimeZone.getTimeZone("IST");
            SimpleDateFormat destFormat = new SimpleDateFormat("dd MMM hh:mm a");
            destFormat.setTimeZone(tz);

            //signal 1
            String signal = mLiveData.getSignalList().get(0).getPos().toUpperCase();

            double buyAboveVal = 0;
            String date = "";
            if (pos.equalsIgnoreCase("buy")) {
                buyAboveVal = mLiveData.getSignalList().get(0).getBuyAbove();
            } else {
                buyAboveVal = mLiveData.getSignalList().get(0).getSellBelow();
            }
            date = mLiveData.getSignalList().get(0).getDate();
            try {
                Date myDate = simpleDateFormat.parse(date);
                date = destFormat.format(myDate);

            }
            catch(ParseException e)
            {
                e.printStackTrace();
            }

            date1.setText(date);
            if(!signal.equalsIgnoreCase("buy"))
            {
                signal1.setImageDrawable(redcircle);
            }
            buyAbove1.setText("₹"+ buyAboveVal);

            //signal 2
            signal = mLiveData.getSignalList().get(1).getPos().toUpperCase();

            if (pos.equalsIgnoreCase("buy")) {
                buyAboveVal = mLiveData.getSignalList().get(1).getBuyAbove();
            } else {
                buyAboveVal = mLiveData.getSignalList().get(1).getSellBelow();
            }
            date = mLiveData.getSignalList().get(1).getDate();

            try {
                Date myDate = simpleDateFormat.parse(date);
                date = destFormat.format(myDate);

            }
            catch(ParseException e)
            {
                e.printStackTrace();
            }

            date2.setText(date);
            if(!signal.equalsIgnoreCase("buy"))
            {
                signal2.setImageDrawable(redcircle);
            }
            buyAbove2.setText("₹"+ buyAboveVal);

            //signal 3
            signal = mLiveData.getSignalList().get(2).getPos().toUpperCase();

            if (pos.equalsIgnoreCase("buy")) {
                buyAboveVal = mLiveData.getSignalList().get(2).getBuyAbove();
            } else {
                buyAboveVal = mLiveData.getSignalList().get(2).getSellBelow();
            }
            date = mLiveData.getSignalList().get(2).getDate();

            try {
                Date myDate = simpleDateFormat.parse(date);
                date = destFormat.format(myDate);
            }
            catch(ParseException e)
            {
                e.printStackTrace();
            }

            date3.setText(date);
            if(!signal.equalsIgnoreCase("buy"))
            {
                signal3.setImageDrawable(redcircle);
            }
            buyAbove3.setText("₹"+ buyAboveVal);



        }

        String symbol = mLiveData.getSymbol();
        //symboltv.setText(" " + symbol/*.substring(0,symbol.length() - 2)*/); //      //Remove '-I' from symbol.

    }

    public static char isShowTick(boolean isTarget) {
        if (isTarget) {
            return '✓';
        } else return ' ';
    }


}

