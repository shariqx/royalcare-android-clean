package com.tradr.royalcare.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.tradr.royalcare.R;
import com.tradr.royalcare.adaptor.LiveDataAdaptor;
import com.tradr.royalcare.model.LiveData;
import com.tradr.royalcare.model.LiveDataResponse;
import com.tradr.royalcare.network.ApiClient;
import com.tradr.royalcare.network.ApiInterface;
import com.tradr.royalcare.util.RecyclerItemClickListener;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Shariq on 6/4/2017.
 */

public class ReportsFragment extends Fragment implements SearchView.OnQueryTextListener, AdapterView.OnItemSelectedListener, Runnable {

    Handler timerHandler;
    private RecyclerView recList;
    public LiveDataAdaptor liveDataAdaptor = null;
    // private List<LiveData> mLiveData;
    private List<LiveData> mLiveData;
    Context me = null;
    View meV = null;
    Bundle meS = null;
    private Parcelable recyclerViewState;
    SearchView searchView;
    SignalPopup currentlyOpenedSignal = null;
    int currentlySelectedSignalPos = 0;
    private TextView datetime;

    public ReportsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        timerHandler = new Handler();
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.stock_futures_fragment, container, false);
        Spinner spinner = (Spinner) view.findViewById(R.id.spinner);
        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
        // Spinner Drop down elements
        List<String> categories = new ArrayList<>();
        //categories.add("All");
        categories.add("Positional");
        categories.add("Intraday");
        categories.add("One Month");
        // Creating adapter for spinner

        datetime = (TextView) view.findViewById(R.id.datetime);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        this.meV = view;
        meS = savedInstanceState;
        recList = (RecyclerView) view.findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        //liveDataAdaptor = new LiveDataAdaptor();
        createList(view.getContext());
        recList.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(final View view, int position) {
                LiveData s = liveDataAdaptor.getmLiveData().get(position);
                if (savedInstanceState == null) {
                    //currentlySelectedSignalPos  = position;
                    currentlyOpenedSignal = SignalPopup.newInstance(s);
                    currentlyOpenedSignal.show(getFragmentManager(), null);
                    return;
                }
            }

        }));

        this.run();
        return view;
    }

    private void createList(final Context context) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        //Call<StockResponse> call = apiService.getPredictionOrSymbolJSON();

        Call<LiveDataResponse> call = apiService.getLiveData();
        call.enqueue(new Callback<LiveDataResponse>() {
            @Override
            public void onResponse(Call<LiveDataResponse> call, Response<LiveDataResponse> response) {
                try {
                    List<LiveData> liveDataList = response.body().getResults();
                    if (liveDataAdaptor == null) {
                        liveDataAdaptor = new LiveDataAdaptor(liveDataList);
                        liveDataAdaptor.setmLiveData(liveDataList);
                        recList.setAdapter(liveDataAdaptor);
                    }
                    mLiveData = liveDataList;

                    //recList.setAdapter(liveDataAdaptor);
                    ((LiveDataAdaptor) recList.getAdapter()).getmLiveData().clear();
                    ((LiveDataAdaptor) recList.getAdapter()).getmLiveData().addAll(mLiveData);

                    //Filter based on search view's selected query
                    if (searchView != null) {
                        String query = String.valueOf(searchView.getQuery()).trim();
                        if (query.length() > 0) {
                            List<LiveData> new_liveData = filter(mLiveData, query);
                            liveDataAdaptor.setmLiveData(new_liveData);
                        }
                    }

                    //04-05-17 Removing this code to wrong data being updated when Signal Popup is opened...
                 /*   if(currentlyOpenedSignal != null && SignalPopup.mLiveData != null && currentlyOpenedSignal.getView() != null ) {
                        LiveData curr = mLiveData.get(currentlySelectedSignalPos);
                        SignalPopup.mLiveData = curr;
                        currentlyOpenedSignal.showPredictionPopup(currentlyOpenedSignal.getView(), curr.getPos());
                    }*/
                    liveDataAdaptor.notifyDataSetChanged();
                    //Toast.makeText(context, "Data set has been changed", Toast.LENGTH_SHORT).show();
                    //recList.setAdapter(new liveDataAdaptor(liveClashData));


                    //set updated date and time
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //2017-02-06T09:43:37.203
                    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                    TimeZone tz = TimeZone.getTimeZone("IST");
                    SimpleDateFormat destFormat = new SimpleDateFormat("dd MMM hh:mm a");
                    destFormat.setTimeZone(tz);

                    try {
                        String updatedtime = mLiveData.get(0).getUpdate_time();
                        Date myDate = simpleDateFormat.parse(updatedtime);
                        String formatted_date = destFormat.format(myDate);

                        datetime.setText("Updated at " + formatted_date);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }



                } catch (Exception ex) {
                    Toast.makeText(context, "Error while retreiving Stocks", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LiveDataResponse> call, Throwable t) {
                // Log error here since request failed
                Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
                Log.d("myapp", "Something went wrong in calling the livedata...", t);
            }
        });
/*
         call.enqueue(new Callback<StockResponse>() {
             @Override
             public void onResponse(Call<StockResponse> call, Response<StockResponse> response) {
                 try {
                     List<LiveData> movies = response.body().getResults();
                     Toast.makeText(context, movies.size() + " Stocks Received", Toast.LENGTH_SHORT).show();
                     liveDataAdaptor = new liveDataAdaptor(movies);
                     recList.setAdapter(liveDataAdaptor);
                     mLiveData = movies;
                     //recList.setAdapter(new liveDataAdaptor(movies));
                 } catch (Exception ex) {
                     Toast.makeText(context, "Error while retreiving Stocks", Toast.LENGTH_SHORT).show();
                 }
             }

             @Override
             public void onFailure(Call<StockResponse> call, Throwable t) {
                 // Log error here since request failed
                 Toast.makeText(context, t.toString(), Toast.LENGTH_SHORT).show();
             }
         });*/
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_stock, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        // Do something when collapsed
                        liveDataAdaptor.setFilter(mLiveData);
                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        if ("".equals(newText.trim())) { //Don't do anything if only spaces are put in.
            return false;
        }
        final List<LiveData> filteredModelList = filter(mLiveData, newText);
        liveDataAdaptor.setFilter(filteredModelList);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    private List<LiveData> filter(List<LiveData> models, String query) {
        query = query.toLowerCase();
        final List<LiveData> filteredModelList = new ArrayList<>();
        for (LiveData model : models) {
            final String text = model.getSymbol().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

    private List<LiveData> filterOnInstrType(List<LiveData> models, String type) {

        final List<LiveData> filteredModelList = new ArrayList<>();
        for (LiveData model : models) {

            filteredModelList.add(model);

        }
        return filteredModelList;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (mLiveData == null)
            return;
        String item = parent.getItemAtPosition(position).toString();
        if (item.equalsIgnoreCase("stock")) {
            final List<LiveData> filteredModelList = filterOnInstrType(mLiveData, "stk");
            liveDataAdaptor.setFilter(filteredModelList);
        } else if (item.equalsIgnoreCase("index")) {
            final List<LiveData> filteredModelList = filterOnInstrType(mLiveData, "idx");
            liveDataAdaptor.setFilter(filteredModelList);
        } else {
            liveDataAdaptor.setFilter(mLiveData);
        }

    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void run() {
        // Here you can update your adapter data
        //yourAdapter.notifyDataSetChanged();
        View view = meV;
        //this.run(); WTF!!!! INCEPTION ?
        createList(view.getContext());
        timerHandler.postDelayed(this, 5000);
    }


    @Override
    public void onStop() {
        try {
            super.onStop();//Wish I knew more about java! :P
            timerHandler.removeCallbacks(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onResume() {
        super.onResume();
        //timerHandler.postDelayed(this, 5000);
        this.run();
    }
}