package com.tradr.royalcare.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.tradr.royalcare.R;
import com.tradr.royalcare.activity.TransactionDetailActivity;
import com.tradr.royalcare.adaptor.TransactionAdaptor;
import com.tradr.royalcare.constant.Constants;
import com.tradr.royalcare.helper.PortfolioHelper;
import com.tradr.royalcare.helper.RecyclerItemTouchHelper;
import com.tradr.royalcare.model.TransactionData;
import com.tradr.royalcare.model.TransactionResponse;
import com.tradr.royalcare.network.ApiClient;
import com.tradr.royalcare.network.ApiInterface;
import com.tradr.royalcare.util.RecyclerItemClickListener;

import org.honorato.multistatetogglebutton.MultiStateToggleButton;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Shariq on 6/5/2017.
 */

public class FuturesFragment extends Fragment implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {
    private TransactionAdaptor transAdaptor;
    private RecyclerView recList;
    private TextView summaryValue;
    private ImageView summary;
    private TextView summaryLabel;
    private static int count;
    private LinearLayout linearLayout;
    private List<TransactionData> movies;
    private Context context;
    static final float OPTIONS_AREA_PROPORTION = 0.5f;
    static final float REMOVE_ITEM_THRESHOLD = 0.6f;
    private RelativeLayout coordinatorLayout;
    private String profpercentage;

    public FuturesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        context = getContext();
        final View view = inflater.inflate(R.layout.portfolio_buy_fragment, container, false);
        count=0;
        //recList.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(this, R.drawable.list_divider), true));

        recList = (RecyclerView) view.findViewById(R.id.portfolio_tansaction_list);
        coordinatorLayout = (RelativeLayout)view.findViewById(R.id.coordinator_layout);
        summaryValue = (TextView) view.findViewById(R.id.summary_value);
        summaryLabel = (TextView) view.findViewById(R.id.summary_label);
        //summary = (ImageView) view.findViewById(R.id.summary);
        linearLayout = (LinearLayout)view.findViewById(R.id.relativelayout);

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count+=1;
                updateSummary();
                Log.d("counthere",count+"");
            }
        });



        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        recList.setItemAnimator(new DefaultItemAnimator());
        recList.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recList);

        // making http call and fetching menu json
        //TODO: Code to call another activity for showing the transaction full details will be called from here
        recList.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(final View view, int position) {
                Intent intent = new Intent(context, TransactionDetailActivity.class);
                intent.putExtra("position",position);
                Log.d("puttinghere",position+"");
                startActivity(intent);

            }
        }));

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        //Call<StockResponse> call = apiService.getPredictionOrSymbolJSON();
        Call<TransactionResponse> call = apiService.getTransactions(Constants.USERID);
        call.enqueue(new Callback<TransactionResponse>() {
            @Override
            public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {
                try {
                    movies = response.body().getTransaction();
                    movies = PortfolioHelper.getGroupedTransactionForListing(movies, getArguments().getString("mode"));
                    movies = PortfolioHelper.getCurrentList();
                    if(movies.size() > 0)
                    {
                        linearLayout.setVisibility(View.VISIBLE);
                    }
                    transAdaptor = new TransactionAdaptor(movies);
                    recList.setAdapter(transAdaptor);
                    updateSummary();


                } catch (Exception ex) {
                    ex.printStackTrace();
                    //Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TransactionResponse> call, Throwable t) {
                // Log error here since request failed
                //Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();
                //Log.d("myapp", "Something", t);
            }
        });

        return view;
    }
    
    private void updateSummary()
    {

        switch(count%3)
        {
            case 0: //profit
                if (movies != null && movies.size() > 0) {
                    double profit = 0;
                    for (TransactionData td : movies) {
                        double temp = Double.parseDouble(td.getLTP()) - Double.parseDouble(td.getPrice());
                        temp *= Double.parseDouble(td.getQuantity());
                        profit +=  temp;
                    }

                    if(profit<0)
                    {
                        profit = Math.abs(profit);
                        summaryValue.setText("₹ "+String.format("%.2f",profit));
                        summaryLabel.setText("Net Loss");
                    }
                    else
                    {
                        summaryValue.setText("₹ "+String.format("%.2f",profit));
                        summaryLabel.setText("Net Profit");
                    }


                }
                break;
            case 1: //profit percentage
                if (movies != null && movies.size() > 0) {
                    double profit = 0;
                    double price = 0;
                    for (TransactionData td : movies) {
                        double temp = Double.parseDouble(td.getLTP()) - Double.parseDouble(td.getPrice());
                        temp *= Double.parseDouble(td.getQuantity());
                        profit = temp;
                        price+=Double.valueOf(td.getLTP());

                    }

                    profit = profit/price*100;
                    profpercentage = profit+"";
                    if(profit<0)
                    {
                        profit = Math.abs(profit);
                        summaryValue.setText(String.format("%.2f",profit)+" % ");
                        summaryLabel.setText("Loss %");
                    }
                    else
                    {
                        summaryValue.setText(String.format("%.2f",profit)+" %");
                        summaryLabel.setText("Profit %");
                    }

                }
                break;

            case 2: //net worth
                if (movies != null && movies.size() > 0) {
                    Log.d("hereiam","here");
                    double price = 0;
                    for (TransactionData td : movies) {
                        price += td.getCurrentCost()*Double.valueOf(td.getQuantity());

                    }

                    summaryValue.setText(String.format("%.2f",price));
                    summaryLabel.setText("Net Worth");

                }
                break;


        }

    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof TransactionAdaptor.TransactionViewHolder) {
            // get the removed item name to display it in snack bar
            String name = movies.get(viewHolder.getAdapterPosition()).getStockName();
            final String symbol = movies.get(viewHolder.getAdapterPosition()).getSymbol();
            // backup of removed item for undo purpose
            final TransactionData deletedItem = movies.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view
            transAdaptor.removeItem(viewHolder.getAdapterPosition());
            // showing snack bar with Undo option
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, name + " removed from Portfolio!", Snackbar.LENGTH_LONG);
            snackbar.setAction("CONFIRM", new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // undo is selected, restore the deleted item
                    //transAdaptor.restoreItem(deletedItem, deletedIndex);

                    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                    Call<ResponseBody> call = apiService.deleteTransactionForUser(Constants.USERID, symbol);
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            Toast.makeText(getContext(),"Transaction deleted successfully",Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(getContext(),"Transaction deletion failed!",Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();

        }
    }

}