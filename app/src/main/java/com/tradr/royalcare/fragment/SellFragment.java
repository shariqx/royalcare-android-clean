package com.tradr.royalcare.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.tradr.royalcare.R;
import com.tradr.royalcare.adaptor.TransactionAdaptor;
import com.tradr.royalcare.constant.Constants;
import com.tradr.royalcare.helper.PortfolioHelper;
import com.tradr.royalcare.model.TransactionData;
import com.tradr.royalcare.model.TransactionResponse;
import com.tradr.royalcare.network.ApiClient;
import com.tradr.royalcare.network.ApiInterface;
import com.tradr.royalcare.util.RecyclerItemClickListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SellFragment extends Fragment {
    private RecyclerView recList;
    private TransactionAdaptor transAdaptor;
    private List<TransactionData> transactionData;
    private LinearLayout relativeLayout;
    public SellFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        {
            setHasOptionsMenu(true);
            // Inflate the layout for this fragment
            final View view = inflater.inflate(R.layout.portfolio_buy_fragment, container, false);
            recList = (RecyclerView) view.findViewById(R.id.portfolio_tansaction_list);
            recList.setHasFixedSize(true);
            LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            recList.setLayoutManager(llm);
            relativeLayout = (LinearLayout) view.findViewById(R.id.relativelayout);
            relativeLayout.setVisibility(View.GONE);

            recList.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(final View view, int position) {
                }
            }));

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            //Call<StockResponse> call = apiService.getPredictionOrSymbolJSON();
            Call<TransactionResponse> call = apiService.getTransactions(Constants.USERID);
            call.enqueue(new Callback<TransactionResponse>() {
                @Override
                public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {
                    try {
                        List<TransactionData> movies = response.body().getTransaction();
                        movies = PortfolioHelper.getGroupedTransactionForListing(movies, getArguments().getString("mode"));
                        movies = PortfolioHelper.getHistoryList();
                        Log.d("movies",movies.toString());
                        if (movies.size() == 0) {
                            //Toast.makeText(view.getContext(), "No Transaction history", Toast.LENGTH_LONG).show();
                        }
                        //Toast.makeText(view.getContext(), "s Stocks Received", Toast.LENGTH_SHORT).show();
                        transAdaptor = new TransactionAdaptor(movies);
                        recList.setAdapter(transAdaptor);
                        transactionData = movies;
                        //recList.setAdapter(new liveDataAdaptor(movies));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        Toast.makeText(getContext(), "Error while retrieving Stocks", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TransactionResponse> call, Throwable t) {
                    // Log error here since request failed
                    Toast.makeText(getContext(), t.toString(), Toast.LENGTH_SHORT).show();
                    Log.d("myapp", "Something", t);
                }
            });

            return view;
        }
    }

}
